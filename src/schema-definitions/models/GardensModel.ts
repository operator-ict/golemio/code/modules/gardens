import { Point } from "@golemio/core/dist/shared/geojson";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { IGarden } from "#sch/Gardens";

export class GardensModel extends Model<IGarden> implements IGarden {
    declare id: string;
    declare geometry: Point;
    declare name: string;

    declare image?: string;
    declare description?: string;
    declare url?: string;
    declare address?: string;
    declare district?: string;
    declare restaurace?: string;
    declare brusle?: string;
    declare cesty?: string;
    declare doba?: string;
    declare hriste?: string;
    declare kolo?: string;
    declare mhd?: string;
    declare misto?: string;
    declare parking?: string;
    declare provoz?: string;
    declare sport?: string;
    declare wc?: string;

    public static attributeModel: ModelAttributes<GardensModel, IGarden> = {
        id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        geometry: DataTypes.GEOMETRY,
        name: DataTypes.STRING,

        image: DataTypes.TEXT,
        description: DataTypes.TEXT,
        url: DataTypes.TEXT,
        address: DataTypes.TEXT,
        district: DataTypes.STRING,
        restaurace: DataTypes.TEXT,
        brusle: DataTypes.TEXT,
        cesty: DataTypes.TEXT,
        doba: DataTypes.TEXT,
        hriste: DataTypes.TEXT,
        kolo: DataTypes.TEXT,
        mhd: DataTypes.TEXT,
        misto: DataTypes.TEXT,
        parking: DataTypes.TEXT,
        provoz: DataTypes.TEXT,
        sport: DataTypes.TEXT,
        wc: DataTypes.TEXT,
    };

    public static jsonSchema: JSONSchemaType<IGarden[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "string" },
                geometry: { $ref: "#/definitions/geometry" },
                name: { type: "string" },

                image: { type: "string", nullable: true },
                description: { type: "string", nullable: true },
                url: { type: "string", nullable: true },
                address: { type: "string", nullable: true },
                district: { type: "string", nullable: true },
                restaurace: { type: "string", nullable: true },
                brusle: { type: "string", nullable: true },
                cesty: { type: "string", nullable: true },
                doba: { type: "string", nullable: true },
                hriste: { type: "string", nullable: true },
                kolo: { type: "string", nullable: true },
                mhd: { type: "string", nullable: true },
                misto: { type: "string", nullable: true },
                parking: { type: "string", nullable: true },
                provoz: { type: "string", nullable: true },
                sport: { type: "string", nullable: true },
                wc: { type: "string", nullable: true },
            },
            required: ["id", "geometry", "name"],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
