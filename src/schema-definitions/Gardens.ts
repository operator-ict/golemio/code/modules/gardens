import { Point } from "@golemio/core/dist/shared/geojson";

export interface IGarden {
    id: string;
    geometry: Point;
    name: string;

    image?: string;
    description?: string;
    url?: string;
    address?: string;
    district?: string;

    restaurace?: string;
    brusle?: string;
    cesty?: string;
    doba?: string;
    hriste?: string;
    kolo?: string;
    mhd?: string;
    misto?: string;
    parking?: string;
    provoz?: string;
    sport?: string;
    wc?: string;

    updated_at?: string;
}

export const gardens = {
    name: "Gardens",
    pgTableName: "gardens",
};
