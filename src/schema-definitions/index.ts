import { gardensDatasource } from "#sch/datasources/GardensJsonSchema";
import { gardens } from "#sch/Gardens";

const forExport: any = {
    name: "Gardens",
    pgSchema: "gardens",
    datasources: {
        gardensDatasource,
    },
    definitions: {
        gardens,
    },
};

export { forExport as Gardens };
