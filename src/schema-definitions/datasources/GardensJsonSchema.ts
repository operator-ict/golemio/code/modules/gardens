import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IGardensInput {
    name: string;
    slug: string;
    coordinates: number[];

    image?: string;
    description?: string;
    url?: string;
    address?: string;
    district?: string;

    properties_restaurace?: string;
    properties_brusle?: string;
    properties_cesty?: string;
    properties_doba?: string;
    properties_hriste?: string;
    properties_kolo?: string;
    properties_mhd?: string;
    properties_misto?: string;
    properties_parking?: string;
    properties_provoz?: string;
    properties_sport?: string;
    properties_wc?: string;
}

const gardensJsonSchema: JSONSchemaType<IGardensInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            name: { type: "string" },
            slug: { type: "string" },
            coordinates: {
                type: "array",
                items: {
                    type: "number",
                },
                minItems: 2,
                maxItems: 2,
            },

            image: { type: "string", nullable: true },
            description: { type: "string", nullable: true },
            url: { type: "string", nullable: true },
            address: { type: "string", nullable: true },
            district: { type: "string", nullable: true },

            properties_restaurace: { type: "string", nullable: true },
            properties_brusle: { type: "string", nullable: true },
            properties_cesty: { type: "string", nullable: true },
            properties_doba: { type: "string", nullable: true },
            properties_hriste: { type: "string", nullable: true },
            properties_kolo: { type: "string", nullable: true },
            properties_mhd: { type: "string", nullable: true },
            properties_misto: { type: "string", nullable: true },
            properties_parking: { type: "string", nullable: true },
            properties_provoz: { type: "string", nullable: true },
            properties_sport: { type: "string", nullable: true },
            properties_wc: { type: "string", nullable: true },
        },
        required: ["name", "slug", "coordinates"],
        additionalProperties: false,
    },
};

export const gardensDatasource: { name: string; jsonSchema: JSONSchemaType<IGardensInput[]> } = {
    name: "GardensDatasource",
    jsonSchema: gardensJsonSchema,
};
