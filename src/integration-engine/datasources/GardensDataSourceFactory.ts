import { Gardens } from "#sch";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

class GardensDataSourceFactory {
    public gardensDataSource: DataSource;

    constructor() {
        this.gardensDataSource = new DataSource(
            Gardens.datasources.gardensDatasource.name,
            new HTTPFetchProtocolStrategy({
                headers: {
                    Authorization: "Basic " + config.datasources.OICTEndpointApikey,
                },
                method: "GET",
                url: config.datasources.Gardens,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                Gardens.datasources.gardensDatasource.name + "Validator",
                Gardens.datasources.gardensDatasource.jsonSchema
            )
        );
    }
}

const gardensDataSourceFactory = new GardensDataSourceFactory();
const gardensDataSource = gardensDataSourceFactory.gardensDataSource;

export { gardensDataSource };
