import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Gardens } from "#sch";
import { GardensModel } from "#sch/models/GardensModel";
import { IGarden } from "#sch/Gardens";

export class GardensRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "GardensRepository",
            {
                outputSequelizeAttributes: GardensModel.attributeModel,
                pgTableName: Gardens.definitions.gardens.pgTableName,
                pgSchema: Gardens.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("GardensRepositoryValidator", GardensModel.jsonSchema)
        );
    }

    public saveBulk = async (data: IGarden[]) => {
        if (data && data.length && (await this.validate(data))) {
            const fieldsToUpdate = Object.getOwnPropertyNames(data[0])
                .filter((el) => el !== "id")
                .map<keyof IGarden>((el) => el as keyof IGarden);
            fieldsToUpdate.push("updated_at" as any);
            await this.sequelizeModel.bulkCreate<GardensModel>(data, {
                updateOnDuplicate: fieldsToUpdate,
            });
        }
    };
}
