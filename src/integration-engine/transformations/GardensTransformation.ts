import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { Gardens } from "#sch";
import { IGarden } from "#sch/Gardens";
import { IGardensInput } from "#sch/datasources/GardensJsonSchema";

export class GardensTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = Gardens.name;
    }

    public transform = async (data: IGardensInput[]): Promise<IGarden[]> => {
        const gardens: IGarden[] = [];

        for (const item of data) {
            const transformedData = await this.transformElement(item);
            gardens.push(transformedData);
        }

        return gardens;
    };

    protected transformElement = (element: IGardensInput): IGarden => {
        const res: IGarden = {
            id: element.slug,
            geometry: {
                coordinates: element.coordinates,
                type: "Point",
            },
            name: element.name,

            image: element.image,
            description: element.description,
            url: element.url,
            address: element.address,
            district: element.district,

            restaurace: element.properties_restaurace,
            brusle: element.properties_brusle,
            cesty: element.properties_cesty,
            doba: element.properties_doba,
            hriste: element.properties_hriste,
            kolo: element.properties_kolo,
            mhd: element.properties_mhd,
            misto: element.properties_misto,
            parking: element.properties_parking,
            provoz: element.properties_provoz,
            sport: element.properties_sport,
            wc: element.properties_wc,
        };
        return res;
    };
}
