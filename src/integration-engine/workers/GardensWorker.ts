import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { RefreshDataInDBTask } from "#ie/workers/tasks/RefreshDataInDBTask";

export class GardensWorker extends AbstractWorker {
    protected readonly name = "Gardens";

    constructor() {
        super();
        this.registerTask(new RefreshDataInDBTask(this.getQueuePrefix()));
    }
}
