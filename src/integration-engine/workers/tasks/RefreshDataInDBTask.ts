import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";

import { gardensDataSource } from "#ie/datasources/GardensDataSourceFactory";
import { GardensTransformation } from "#ie";
import { GardensRepository } from "#ie/repositories/GardensRepository";

export class RefreshDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refreshDataInDB";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours

    private dataSource: DataSource;
    private transformation: GardensTransformation;
    private repository: GardensRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSource = gardensDataSource;
        this.transformation = new GardensTransformation();
        this.repository = new GardensRepository();
    }

    protected async execute(): Promise<void> {
        const data = await this.dataSource.getAll();
        const transformedData = await this.transformation.transform(data);
        await this.repository.saveBulk(transformedData);
    }
}
