import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { IGeoJsonModel } from "@golemio/core/dist/output-gateway/models/interfaces/IGeoJsonModel";
import {
    buildGeojsonFeatureCollection,
    buildGeojsonFeatureLatLng,
    IGeoJsonAllFilterParameters,
    IGeoJSONFeature,
    IGeoJSONFeatureCollection,
} from "@golemio/core/dist/output-gateway";
import { FilterHelper } from "./FilterHelper";

import { Gardens } from "#sch";
import { GardensModel } from "#sch/models/GardensModel";

interface IRestProps {
    restaurace?: string;
    brusle?: string;
    cesty?: string;
    doba?: string;
    hriste?: string;
    kolo?: string;
    mhd?: string;
    misto?: string;
    parking?: string;
    provoz?: string;
    sport?: string;
    wc?: string;
}

export class GardensRepository extends SequelizeModel implements IGeoJsonModel {
    constructor() {
        super("GardensRepository", Gardens.definitions.gardens.pgTableName, GardensModel.attributeModel, {
            schema: Gardens.pgSchema,
        });
    }

    public IsPrimaryIdNumber(idKey: string): Promise<boolean> {
        return Promise.resolve(false);
    }

    public PrimaryIdentifierSelection(id: string): object {
        return { id };
    }

    public GetProperties = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };

    public GetAll = async (options: IGeoJsonAllFilterParameters = {}): Promise<IGeoJSONFeatureCollection> => {
        const result = await this.sequelizeModel.findAll<GardensModel>({
            attributes: {
                include: ["updated_at"],
            },
            where: options
                ? {
                      [Sequelize.Op.and]: [
                          ...FilterHelper.prepareFilterForLocation(options),
                          ...FilterHelper.prepareFilterForUpdateSince(options),
                          ...FilterHelper.prepareFilterForDistricts(options),
                      ],
                  }
                : {},
            limit: options?.limit,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order: FilterHelper.prepareOrderFunction(options),
        });

        return buildGeojsonFeatureCollection(
            result.map((record: GardensModel) => {
                return this.formatOutput(record);
            })
        );
    };

    public GetOne = async (id: string): Promise<IGeoJSONFeature | undefined> => {
        const result = await this.sequelizeModel.findOne<GardensModel>({
            attributes: {
                include: ["updated_at"],
            },
            where: { id },
        });

        return result ? this.formatOutput(result) : undefined;
    };

    private formatOutput = (record: GardensModel): IGeoJSONFeature | undefined => {
        const { geometry, address, image, description, district, id, name, updated_at, url, ...rest } = record.get({
            plain: true,
        });

        return buildGeojsonFeatureLatLng(
            {
                address: {
                    address_formatted: address,
                },
                image: {
                    url: image,
                },
                description,
                district,
                id,
                name,

                properties: this.formatOutputProperties(rest),

                updated_at,
                url,
            },
            geometry.coordinates[0],
            geometry.coordinates[1]
        );
    };

    private formatOutputProperties = (restPropsObject: IRestProps) => {
        const resultPropertiesArr = Object.entries(restPropsObject)
            .map(([propertyId, value]) => {
                if (value) {
                    return {
                        description: this.getPropertyDescription(propertyId),
                        id: propertyId,
                        value,
                    };
                }
                return null;
            })
            .filter((item) => item);
        return resultPropertiesArr;
    };

    private getPropertyDescription = (id: string): string | null => {
        switch (id) {
            case "restaurace":
                return "Občerstvení";
            case "wc":
                return "WC";
            case "misto":
                return "Zajímavosti";
            case "kolo":
                return "Cyklostezky";
            case "hriste":
                return "Dětské hřiště";
            case "brusle":
                return "Bruslení";
            case "sport":
                return "Sportovní hřiště";
            case "mhd":
                return "MHD";
            case "parking":
                return "Parkování";
            case "cesty":
                return "Povrch cest";
            case "provoz":
                return "Provozovatel";
            case "doba":
                return "Otevírací doba";
            default:
                return null;
        }
    };
}
