import { Router } from "@golemio/core/dist/shared/express";
import { GeoJsonRouter } from "@golemio/core/dist/output-gateway/routes";
import { GardensRepository } from ".";

export class GardensRouter extends GeoJsonRouter {
    constructor() {
        super(new GardensRepository());
        this.initRoutes({ maxAge: 12 * 60 * 60, staleWhileRevalidate: 60 * 60 });
    }
}

const gardensRouter: Router = new GardensRouter().router;

export { gardensRouter };
