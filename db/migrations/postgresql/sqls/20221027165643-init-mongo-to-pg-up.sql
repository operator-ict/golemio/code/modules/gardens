CREATE TABLE IF NOT EXISTS gardens (
    "id" varchar(150) NOT NULL,
    "geometry" geometry NOT NULL,
    "name" varchar(150) NOT NULL,
    "image" text,
    "description" text,
    "url" text,
    "address" text,
    "district" varchar(150), 
    "restaurace" text,
    "brusle" text,
    "cesty" text,
    "doba" text,
    "hriste" text,
    "kolo" text,
    "mhd" text,
    "misto" text,
    "parking" text,
    "provoz" text,
    "sport" text,
    "wc" text,


    -- audit fields,
    create_batch_id int8,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id int8,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT gardens_pkey PRIMARY KEY ("id")
);

CREATE INDEX idx_gardens_geom ON gardens USING gist ("geometry");
CREATE INDEX gardens_district_idx ON gardens USING btree ("district");
CREATE INDEX gardens_updated_at_idx ON gardens USING btree ("updated_at");