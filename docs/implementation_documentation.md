# Implementační dokumentace modulu _gardens_

## Záměr

Modul slouží k ukládání a poskytování informací o Gardens.

## Vstupní data

### Stahujeme data ze dvou zdrojů, zpracováváme je a ukládáme do samostatných tabulek.

#### _Protected endpoint at mojepraha.eu_

-   zdroj dat
    -   url: [config.datasources.Gardens](https://www.mojepraha.eu/api/gardens)
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [gardensJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/gardens/-/blob/development/src/integration-engine/datasources/GardensDataSource.ts)
    -   příklad [vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/gardens/-/blob/development/test/integration-engine/data/gardens-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.gardens.refreshDataInDB
            -   rabin `0 23 7 * * *`
            -   prod `0 20 2 * * 5`
-   název rabbitmq fronty
    -   dataplatform.gardens.refreshDataInDB

### Gardens

#### _task: RefreshDataInDBTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.gardens.refreshDataInDB
    -   bez parametrů
-   datové zdroje
    -   dataSource mojepraha.eu
-   transformace
    -   [GardensTransformation.ts](https://gitlab.com/operator-ict/golemio/code/modules/gardens/-/blob/development/src/integration-engine/transformations/GardensTransformation.ts) - mapování pro `GardensModel`

## Uložení dat

-   typ databáze
    -   PSQL
-   databázové schéma
    -   ![gardens er diagram](./assets/gardens_erd.png)

## Output API

`GardensRouter` implementuje `GeoJsonRouter`.

### Obecné

-   OpenAPI v3 dokumentace
    -   [OpenAPI](./openapi.yaml)
-   api je veřejné
-   postman kolekce
    -   TBD
-   Asyncapi dokumentace RabbitMQ front
    -   [AsyncAPI](./asyncapi.yaml)

#### /gardens

-   zdrojové tabulky
    -   `gardens`
-   dodatečná transformace: Feature collection

#### /gardens/:id

-   zdrojové tabulky
    -   `gardens`
-   dodatečná transformace: Feature
