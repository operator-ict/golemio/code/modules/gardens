import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { createSandbox, SinonSandbox } from "sinon";
import { GardensRepository } from "#og";

chai.use(chaiAsPromised);

describe("GardensRepository", () => {
    let sandbox: SinonSandbox;
    let gardensRepository: GardensRepository;

    before(() => {
        sandbox = createSandbox();
        gardensRepository = new GardensRepository();
    });

    after(() => {
        sandbox.restore();
    });

    it("should instantiate", () => {
        expect(gardensRepository).not.to.be.undefined;
    });

    it("should return all items", async () => {
        const result = await gardensRepository.GetAll();
        expect(result).to.be.an.instanceOf(Object);
        expect(result.features).to.be.an.instanceOf(Array);
        expect(result.features).to.have.length(4);
    });

    it("should return single item", async () => {
        const id = "bertramka";
        const result = await gardensRepository.GetOne(id);
        expect(result).not.to.be.empty;
        expect(result!.properties).to.have.property("id", id);
        expect(result!.properties).to.have.property("name", "Bertramka");
    });
});
