import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";

import { gardensRouter } from "#og/GardensRouter";
import { gardensOutputDataFixture } from "../integration-engine/data/gardens-output";

chai.use(chaiAsPromised);

describe("Gardens Router", () => {
    const app = express();

    before(async () => {
        app.use("/gardens", gardensRouter);

        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /gardens", (done) => {
        request(app).get("/gardens").set("Accept", "application/json").expect("Content-Type", /json/).expect(200, done);
    });

    it("should respond correctly to GET /gardens/:id", (done) => {
        request(app)
            .get("/gardens/bertramka")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.equal(gardensOutputDataFixture);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /gardens with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/gardens")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /gardens/:id with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/gardens/bertramka")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });
});
