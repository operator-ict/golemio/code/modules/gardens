import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFileSync } from "fs";
import { GardensTransformation } from "#ie/transformations/GardensTransformation";
import { IGarden } from "#sch/Gardens";

chai.use(chaiAsPromised);

describe("GardensTransformation", () => {
    let transformation: GardensTransformation;
    let testSourceData: any[];

    const transformedDataFixture: IGarden = {
        address: "Mozartova 844/14, 15000 Praha, Česko",
        brusle: "NE",
        cesty: "Dlažba",
        description: "Mozartova busta v zahradě legendární usedlosti stojí na vyvýšeném místě.",
        district: "praha-5",
        doba: "Celoročně duben až říjen 9-18 hod., listopad až březen 9:30-16 hod.",
        geometry: {
            coordinates: [14.395004, 50.070894],
            type: "Point",
        },
        hriste: "NE",
        id: "bertramka",
        image: "http://www.praha.eu/public/5c/7c/31/96820_4_bertramka_08.jpg",
        kolo: "ANO",
        mhd: "Bertramka tram č. 4, 7, 9, 10, 58, 59",
        misto: "Kulturní památka, koncerty vážné hudby, společenské akce, muzeum W. A. Mozarta",
        name: "Bertramka",
        parking: "ANO, v podzemním parkingu OC Smíchov",
        provoz: "Městská část Praha 5 a spol. Comenius",
        restaurace: "",
        sport: "NE",
        url: "http://www.praha.eu/jnp/cz/co_delat_v_praze/parky/bertramka/bertramka_text.html",
        wc: "ANO",
    };

    beforeEach(async () => {
        transformation = new GardensTransformation();
        testSourceData = JSON.parse(readFileSync(__dirname + "/../data/gardens-datasource.json", "utf8"));
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("Gardens");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should correctly transform element", async () => {
        const data = await transformation["transformElement"](testSourceData[0]);
        expect(data).to.deep.equal(transformedDataFixture);
    });

    it("should correctly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        for (const item of data) {
            expect(item).to.have.property("geometry");
            expect(item).to.have.property("id");
            expect(item).to.have.property("name");
        }
    });
});
