import { createSandbox, SinonSandbox } from "sinon";
import { expect } from "chai";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";

import { GardensWorker } from "#ie/workers/GardensWorker";

describe("GardensWorker", () => {
    let sandbox: SinonSandbox;
    let worker: GardensWorker;

    beforeEach(() => {
        sandbox = createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        worker = new GardensWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".gardens");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("Gardens");
            expect(result.queues.length).to.equal(1);
        });
    });

    describe("registerTask", () => {
        it("should have 1 task registered", () => {
            expect(worker["queues"].length).to.equal(1);
            expect(worker["queues"][0].name).to.equal("refreshDataInDB");
            expect(worker["queues"][0].options.messageTtl).to.equal(82800000);
        });
    });
});
