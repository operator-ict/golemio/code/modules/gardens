/* eslint-disable max-len */
import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { RefreshDataInDBTask } from "#ie/workers/tasks/RefreshDataInDBTask";

import { IGarden } from "#sch/Gardens";
import { IGardensInput } from "#sch/datasources/GardensJsonSchema";

describe("RefreshDataInDBTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshDataInDBTask;
    let testData: IGardensInput[];
    let testTransformedData: IGarden[];

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        task = new RefreshDataInDBTask("test.gardens");

        testData = [
            {
                name: "Bertramka",
                slug: "bertramka",
                image: "http://www.praha.eu/public/5c/7c/31/96820_4_bertramka_08.jpg",
                description: "Mozartova busta v zahradě legendární usedlosti stojí na vyvýšeném místě.",
                url: "http://www.praha.eu/jnp/cz/co_delat_v_praze/parky/bertramka/bertramka_text.html",
                properties_restaurace: "",
                properties_wc: "ANO",
                properties_misto: "Kulturní památka, koncerty vážné hudby, společenské akce, muzeum W. A. Mozarta",
                properties_kolo: "ANO",
                properties_hriste: "NE",
                properties_brusle: "NE",
                properties_sport: "NE",
                properties_mhd: "Bertramka tram č. 4, 7, 9, 10, 58, 59",
                properties_parking: "ANO, v podzemním parkingu OC Smíchov",
                properties_cesty: "Dlažba",
                properties_provoz: "Městská část Praha 5 a spol. Comenius",
                properties_doba: "Celoročně duben až říjen 9-18 hod., listopad až březen 9:30-16 hod.",
                address: "Mozartova 844/14, 15000 Praha, Česko",
                coordinates: [14.395004, 50.070894],
                district: "praha-5",
            },
        ];

        testTransformedData = [
            {
                address: "Mozartova 844/14, 15000 Praha, Česko",
                brusle: "NE",
                cesty: "Dlažba",
                description: "Mozartova busta v zahradě legendární usedlosti stojí na vyvýšeném místě.",
                district: "praha-5",
                doba: "Celoročně duben až říjen 9-18 hod., listopad až březen 9:30-16 hod.",
                geometry: {
                    coordinates: [14.395004, 50.070894],
                    type: "Point",
                },
                hriste: "NE",
                id: "bertramka",
                image: "http://www.praha.eu/public/5c/7c/31/96820_4_bertramka_08.jpg",
                kolo: "ANO",
                mhd: "Bertramka tram č. 4, 7, 9, 10, 58, 59",
                misto: "Kulturní památka, koncerty vážné hudby, společenské akce, muzeum W. A. Mozarta",
                name: "Bertramka",
                parking: "ANO, v podzemním parkingu OC Smíchov",
                provoz: "Městská část Praha 5 a spol. Comenius",
                restaurace: "",
                sport: "NE",
                url: "http://www.praha.eu/jnp/cz/co_delat_v_praze/parky/bertramka/bertramka_text.html",
                wc: "ANO",
            },
        ];

        sandbox.stub(task["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(task["repository"], "saveBulk").callsFake(() => Promise.resolve());
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"]();

        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["repository"].saveBulk as SinonSpy);
        sandbox.assert.calledWith(task["repository"].saveBulk as SinonSpy, testTransformedData);

        sandbox.assert.callOrder(
            task["dataSource"].getAll as SinonSpy,
            task["transformation"].transform as SinonSpy,
            task["repository"].saveBulk as SinonSpy
        );
    });
});
