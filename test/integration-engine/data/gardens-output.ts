export const gardensOutputDataFixture = {
    geometry: {
        coordinates: [14.395004, 50.070894],
        type: "Point",
    },
    properties: {
        address: {
            address_formatted: "Mozartova 844/14, 15000 Praha, Česko",
        },
        description: "Mozartova busta v zahradě legendární usedlosti stojí na vyvýšeném místě.",
        district: "praha-5",
        id: "bertramka",
        image: {
            url: "http://www.praha.eu/public/5c/7c/31/96820_4_bertramka_08.jpg",
        },
        name: "Bertramka",
        properties: [
            {
                description: "Bruslení",
                id: "brusle",
                value: "NE",
            },
            {
                description: "Povrch cest",
                id: "cesty",
                value: "Dlažba",
            },
            {
                description: "Otevírací doba",
                id: "doba",
                value: "Celoročně duben až říjen 9-18 hod., listopad až březen 9:30-16 hod.",
            },
            {
                description: "Dětské hřiště",
                id: "hriste",
                value: "NE",
            },
            {
                description: "Cyklostezky",
                id: "kolo",
                value: "ANO",
            },
            {
                description: "MHD",
                id: "mhd",
                value: "Bertramka tram č. 4, 7, 9, 10, 58, 59",
            },
            {
                description: "Zajímavosti",
                id: "misto",
                value: "Kulturní památka, koncerty vážné hudby, společenské akce, muzeum W. A. Mozarta",
            },
            {
                description: "Parkování",
                id: "parking",
                value: "ANO, v podzemním parkingu OC Smíchov",
            },
            {
                description: "Provozovatel",
                id: "provoz",
                value: "Městská část Praha 5 a spol. Comenius",
            },
            {
                description: "Sportovní hřiště",
                id: "sport",
                value: "NE",
            },
            {
                description: "WC",
                id: "wc",
                value: "ANO",
            },
        ],
        updated_at: "2022-10-27T16:26:14.685Z",
        url: "http://www.praha.eu/jnp/cz/co_delat_v_praze/parky/bertramka/bertramka_text.html",
    },
    type: "Feature",
};
